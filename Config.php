<?php

namespace Repono;

/**
 * Classe de gerenciamento do cache dos componentes.
 * 
 * @version		0.1
 * @author		Valdirene da Cruz Neves Júnior <vaneves@vaneves.com>
 * @package		Repono
 * @copyright	(c) 2013, Valdirene da Cruz Neves Júnior
 */
class Config
{
	private static $configs = array();
	
	public static function set($key, $value)
	{
		self::$configs[$key] = $value;
	}
	
	public static function get($key)
	{
		if(isset(self::$configs[$key]))
			return self::$configs[$key];
	}
}