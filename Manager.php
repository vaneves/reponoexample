<?php

namespace Repono;

/**
 * Classe de gerenciamento dos componentes.
 * 
 * @version		0.1
 * @author		Valdirene da Cruz Neves Júnior <vaneves@vaneves.com>
 * @package		Repono
 * @copyright	(c) 2013, Valdirene da Cruz Neves Júnior
 */
class Manager
{
	/**
	 * Verifica se há uma versão a ser atualizada de um determinado componente.
	 * @param	string	$name		Nome único do componente.
	 * @param	string	$version	Versão do componente.
	 * @return	
	 */
	public function check($name, $version = 'last')
	{
		$url = rtrim(Config::get('url_repository'), '/') . '/api/check/' . $name . '/' . $version;
		$cache = Cache::getInstance();
		if($cache->has($url))
			return $cache->read($url);
		$response = $this->connect($url);
		if($response)
		{
			$object = json_decode($response);
			$cache->write($url, $object, Config::get('time_cache'));
			
			return $object;
		}
	}
	
	/**
	 * 
	 * @param type $name
	 * @param type $version
	 * @throws Exception
	 */
	public function download($name, $version = 'last')
	{
		$url = rtrim(Config::get('url_repository'), '/') . '/api/download/' . $name . '/' . $version;
		
		$data = $this->connect($url);
		if($data)
		{
			if($data !== 'notfound')
			{
				$directory = Config::get('directory_component');
		
				$tmp = Config::get('directory_component') . '_tmp/';

				if(!is_dir($tmp))
				{
					if(!mkdir($tmp, 0755))
						throw new \Exception('Não foi possível criar o diretório "'. $tmp .'"');
				}
				
				$file = $tmp . md5(uniqid()) . '.zip';
				if(file_put_contents($file, $data))
				{
					$zip = new \ZipArchive();
					if($zip->open($file))
					{
						if(!$zip->extractTo($directory))
							throw new \Exception('Erro ao tentar extrair o componente  em "' . $directory . '"');
					}
					$zip->close();
					unlink($file);
					$this->requires($name);
				}
				else
				{
					throw new \Exception('Erro ao tentar criar o arquivo "' . $file . '"');
				}
			}
			else
			{
				throw new \Exception('O componente "' . $name . '" não foi encontrado');
			}
		}
		else
		{
			throw new \Exception('O repositório retornou uma resposta vazia para o componente "' . $name . '"');
		}
	}
	
	/**
	 * 
	 * @param type $name
	 * @param type $version
	 * @throws Exception
	 */
	public function update($name, $version = 'last')
	{
		$directory = Config::get('directory_component') . $name . '/';
		if($this->destroy($directory))
		{
			$this->download($name, $version);
		}
		else
		{
			throw new \Exception('Erro ao tentar remover os arquivos do componente "' . $name . '"');
		}
	}
	
	private function requires($name)
	{
		$directory = Config::get('directory_component') . $name . '/';
		if(!file_exists($directory . 'component.json'))
			throw new \Exception('Arquivo "'. $name .'/component.json" não foi encontrado');
		$metada = json_decode(file_get_contents($directory . '/component.json'));
		if(!is_object($metada))
			throw new \Exception('Arquivo "'. $name .'/component.json" está vazio');
		
		foreach($metada->requires as $m)
		{
			$d = Config::get('directory_component') . $m->name . '/';
			if(!is_dir($d))
				$this->download($m->name, $m->version);
		}
	}
	
	/**
	 * 
	 * @param type $name
	 * @param type $version
	 * @throws Exception
	 */
	private function connect($url)
	{
		if(!function_exists('curl_init'))
			throw new \Exception ('Para utilizar as classes cliente do Repono é necessário habilitar o "curl"');
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
		$error_code = curl_errno($curl);
		$error_message = curl_error($curl);
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		
		if($error_code)
			throw new \Exception('cURL error: ['. $error_code .']' . $error_message);
		
		if($status == '404')
			throw new \Exception('Repositório "'. Config::get('url_repository') .'" não encontrado');
		
		if(!$response)
			throw new \Exception('A conexão retornou um resultado vazio');
		
		return $response;
	}
	
	private function destroy($dir)
	{
		if (!is_dir($dir) || is_link($dir))
			return unlink($dir);
		foreach (scandir($dir) as $file) {
			if ($file == '.' || $file == '..')
				continue;
			if (!destroy_dir($dir . DIRECTORY_SEPARATOR . $file)) {
				chmod($dir . DIRECTORY_SEPARATOR . $file, 0777);
				if (!destroy_dir($dir . DIRECTORY_SEPARATOR . $file))
					return false;
			};
		}
		return rmdir($dir);
	}
}