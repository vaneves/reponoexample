<?php
namespace Repono;

/**
 * Classe para utilização dos componentes,
 * 
 * @version		0.1
 * @author		Valdirene da Cruz Neves Júnior <vaneves@vaneves.com>
 * @package		Repono
 * @copyright	(c) 2013, Valdirene da Cruz Neves Júnior
 * @abstract
 */
abstract class Repono
{
	/**
	 * 
	 * @param	string	$name
	 * @param	string	$version
	 */
	public static function get($name, $version = 'last')
	{
		$manager = new Manager();
		
		$directory = Config::get('directory_component') . $name;
		
		if(!is_dir($directory))
		{
			$manager->download($name, $version);
			return $name;
		}
		$version = $manager->check($name, $version);
		if($version === -1)
			$manager->update($name, $version);
		return $name;
	}
	
	public static function getPath($name, $version = 'last')
	{
		$path = Config::get('url_component');
		return $path . self::get($name, $version);
	}
	
	public static function getContent($name, $version, $file)
	{
		$name = self::get($name, $version);
		$directory = Config::get('directory_component') . $name;
		return file_get_contents($directory . '/' . $file);
	}
	
	public static function import($name, $version, $file)
	{
		$name = self::get($name, $version);
		$directory = Config::get('directory_component') . $name;
		return include($directory . '/' . $file);
	}
}